//
//  SGZApiRepo.swift
//  github-stargazers
//
//  Created by Andrea Bozza on 02/04/2021.
//

import Foundation
 



private enum StargazerError: Error {
    case responseError
    case noData
    case wrongUrl
}

extension StargazerError: LocalizedError {
    public var errorDescription: String? {
        switch self {
        case .responseError:
            return "Invalid response"
        case .noData:
            return "No data"
        case .wrongUrl:
            return "Unexpected URL / not valid url"
        }
    }
}

enum Result<Value> {
    case success(Value, URLResponse?)
    case failure(Error)
}

class SGZApiRepo {
    var session: URLSession!
    var cachedUrl: URL?
    

    
    init() {
        session = URLSession(configuration: .default)
    }
    
    /**
     Get stargazer list
     
     - Parameters:
        - owner: repository owner es. Alamofire
        - repo : name of repository es. Alamofire
     */
    
    func getStargazers(owner: String, repo: String, completion: @escaping ((Result<[Stargazer]>) -> Void)) {
        let apiUrl = apiBaseURL?
            .appendingPathComponent(repos)
        .appendingPathComponent(owner)
        .appendingPathComponent(repo)
        .appendingPathComponent(stragazers)
        
        guard let url = apiUrl else {
                    return completion(.failure(StargazerError.wrongUrl))
        }
        session.dataTask(with: url) { (data, response, error) in
            DispatchQueue.main.async {
                completion(self.decode(data, response: response, error: error))
                      }
        }.resume()
    }
    
    /**
     Get next page of a stargazer list
     
     - Parameters:
           - nextPage: Next page url of a stargazer list es. https://api.github.com/repositories/1296269/stargazers?page=2
     
     */
    
    func getNextStargazers(nextPage: URL, completion: @escaping ((Result<[Stargazer]>) -> Void)) {
        session.dataTask(with: nextPage) { (data, response, error) in
            DispatchQueue.main.async {
                completion(self.decode(data, response: response, error: error))
                      }
        }.resume()
    }
    
    
    func decode(_ data: Data?, response: URLResponse?, error: Error?) -> (Result<[Stargazer]>) {
        if error != nil {
            return (.failure(StargazerError.responseError))
        }

        guard let jsonData = data else {
            return (.failure(StargazerError.noData))
        }

        do {
            let stargazers = try JSONDecoder().decode([Stargazer].self, from: jsonData)
            return (.success(stargazers, response))
        } catch {
            return (.failure(error))
        }
    }

}

  

