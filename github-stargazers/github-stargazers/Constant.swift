//
//  Constant.swift
//  github-stargazers
//
//  Created by Andrea Bozza on 05/04/2021.
//

import Foundation

open class Constants{
    
    struct Strings {
        static let undefined = "undefined"
        static let empty = ""
        static let invalidStrings = "The username inserted or reponame inserted are empty, please provide a valid user and repo name."
        static let ok = "ok"
        static let listEmpty = "list is Empty"
        static let paginationProblemOccurssMsg = "Pagination error. Please retry again."
        static let serviceNotWorkingMessage = "Invalid username or reponame, please retry again."
        static let noMoreData = "No more data."
        static let appName = "Stargazer"
    }
    
    struct Error {
        static let attention = "Attention!!"
    }
    
    struct Images {
        static let placeholder = "placeholder"
    }
    
    struct Nibs {
        static let  LoadingCell = "LoadingCell"
    }
    
    struct CellIdentifiers{
        static let stargazerCell = "stargazerCellId"
        static let tableviewloadingcell = "loadingcellid"
    }
}
