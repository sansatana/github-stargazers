//
//  StargazersPresenterImpl.swift
//  github-stargazers
//
//  Created by Andrea Bozza on 04/04/2021.
//

import Foundation

class StargazersPresenterImpl<T: StargazerView>:StargazerPresenter {
    
    weak var view : T?
    fileprivate var startgazers = [Stargazer]()
    fileprivate var nextPageURL : String?
    var noMoreData = false
    required init(view: T) {
        self.view = view
    }
    
    func getData(username:String?, reponame:String?) {
        noMoreData = false
        if let user = username, let repo = reponame{
            self.getStargazersFromAPI(username: user, reponame: repo)
        }
    }
    
    func getObjectAt(index: IndexPath) -> StargazerViewModel {
        let stargazer = self.startgazers[index.row]
        return StargazerViewModel(title:stargazer.login, subtitle: stargazer.html_url, imageUrl:stargazer.avatar_url)
    }
    
    func numberOfSections() -> Int {
        return 2
    }
    
    func numberOfRowsInSection(section: Int) -> Int {
        return self.startgazers.count
    }
    
    func loadNextPage(){
        self.getNextPageData()
    }
}

extension StargazersPresenterImpl{
    
    fileprivate func getStargazersFromAPI(username:String!, reponame:String!){
        self.view?.showLoading()
        
        SGZApiRepo().getStargazers(owner: username, repo: reponame) { result in
            self.view?.hideLoading()
            switch result {
            case .failure(_):
                self.view?.hideTableView()
                self.view?.showMessage(message: Constants.Strings.serviceNotWorkingMessage)
            case .success( let stargazers, let response):
                
                let headerFields = (response as? HTTPURLResponse)?.allHeaderFields
                if let headerFields = headerFields{
                    self.nextPageURL  = getNextPageUrl(linkHeader: headerFields["Link"] as? String)
                }
                
                self.startgazers = stargazers
                
                if self.startgazers.isEmpty {
                    self.view?.hideTableView()
                    self.view?.showMessage(message: Constants.Strings.listEmpty)
                }else{
                    self.view?.showTableView()
                }
                self.view?.reloadData()
            }
        }
    }
    //Pagination
    fileprivate func getNextPageData(){
        
        if let pageUrl = self.nextPageURL{
             
            SGZApiRepo().getNextStargazers(nextPage: URL(string: pageUrl)!)  { result in
            
                switch result {
                case .failure(_):
                    self.view?.showMessage(message: Constants.Strings.paginationProblemOccurssMsg)
                case .success( let stargazers, let response):
                    
                    let headerFields = (response as? HTTPURLResponse)?.allHeaderFields
                    if let headerFields = headerFields{
                        self.nextPageURL  = getNextPageUrl(linkHeader: headerFields["Link"] as? String)
                    }
                    
                    self.startgazers.append(contentsOf: stargazers)
                    self.view?.reloadData()
                }
            }
        }else{
            if !self.noMoreData{
                DispatchQueue.main.async() { () -> Void in
                    self.view?.showMessage(message: Constants.Strings.noMoreData)
                    self.noMoreData = true
                }
            }
        }
    }
}
