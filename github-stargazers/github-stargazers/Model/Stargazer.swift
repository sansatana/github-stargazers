//
//  Stargazer.swift
//  github-stargazers
//
//  Created by Andrea Bozza on 04/04/2021.
//


import Foundation

class Stargazer: Codable, Identifiable {
    let login: String
    let avatar_url: String
    var html_url: String?
}
