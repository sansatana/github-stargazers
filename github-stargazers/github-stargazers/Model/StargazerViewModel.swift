//
//  StargazerViewModel.swift
//  github-stargazers
//
//  Created by Andrea Bozza on 04/04/2021.
//


import Foundation

public struct StargazerViewModel{
    var title: String?
    var subtitle: String?
    var imageUrl: String?
}
