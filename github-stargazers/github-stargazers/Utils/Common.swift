//
//  Common.swift
//  github-stargazers
//
//  Created by Andrea Bozza on 04/04/2021.
//

import Foundation

func getNextPageUrl(linkHeader: String?) -> String?{
    
    if let link = linkHeader{
        let links = link.components(separatedBy: ",")
        var dictionary: [String: String] = [:]
        links.forEach({
            let components = $0.components(separatedBy:"; ")
            var cleanPath = components[0].trimmingCharacters(in: CharacterSet(charactersIn: "<>")).trimmingCharacters(in: .whitespacesAndNewlines)
            if cleanPath.hasPrefix("<"){
                cleanPath.remove(at: cleanPath.startIndex)
            }
            if cleanPath.hasSuffix(">"){
                cleanPath.remove(at: cleanPath.endIndex)
            }
            dictionary[components[1]] = cleanPath
        })
        
        if let nextPagePath = dictionary["rel=\"next\""] {
            print("nextPagePath: \(nextPagePath)")
            return nextPagePath
        }
    }
    
    return nil
}


extension String{
    public func isValid() -> Bool{
        return !self.isEmpty
    }
}
