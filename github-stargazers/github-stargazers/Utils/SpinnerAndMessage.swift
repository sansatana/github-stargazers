//
//  SpinnerAndMessage.swift
//  github-stargazers
//
//  Created by Andrea Bozza on 06/04/2021.
//



import Foundation
import UIKit

class SpinnerAndMessage{
    static let sharedInstance = SpinnerAndMessage()
    var activityView: UIActivityIndicatorView!
}

extension SpinnerAndMessage{
    func showSpinnerInView(view: UIView){
        activityView = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.medium)
        activityView.center = view.center
        activityView.hidesWhenStopped = true
        activityView.startAnimating()
        view.addSubview(activityView!)
    }
    
    func hideSpinnerInView(view: UIView){
        if (activityView != nil){
            activityView?.stopAnimating()
        }
    }
    
    func showMessage(title: String, message: String, viewController: UIViewController) {
        let alertController = UIAlertController(
            title: title,
            message: message,
            preferredStyle: .alert
        )
        alertController.addAction(
            UIAlertAction(
                title: NSLocalizedString(Constants.Strings.ok, comment: ""),
                style: .cancel
            )
        )
        viewController.present(alertController, animated: true, completion: nil)
    }
}


