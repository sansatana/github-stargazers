//
//  Configurations.swift
//  github-stargazers
//
//  Created by Andrea Bozza on 03/04/2021.
//

import Foundation

let apiBaseURL = URL(string: "https://api.github.com")

let repos = "repos"
let stragazers = "stargazers"
