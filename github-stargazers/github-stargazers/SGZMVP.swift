//
//  SGZMVP.swift
//  github-stargazers
//
//  Created by Andrea Bozza on 04/04/2021.
//

import Foundation

protocol StargazerView: class {
    func showLoading()
    func hideLoading()
    func reloadData()
    func hideTableView()
    func showTableView()
    func showMessage(message: String)
}

protocol StargazerPresenter{
    func getData(username:String?, reponame:String?)
    func numberOfSections() -> Int
    func numberOfRowsInSection(section: Int) -> Int
    func getObjectAt(index: IndexPath) -> StargazerViewModel
    func loadNextPage()
}
