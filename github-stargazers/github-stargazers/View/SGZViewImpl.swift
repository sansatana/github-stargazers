//
//  StargazersViewImpl.swift
//  github-stargazers
//
//  Created by Andrea Bozza on 04/04/2021.
//


import Foundation
import UIKit

class SGZViewImpl: UIViewController, StargazerView, UITableViewDelegate, UITableViewDataSource{
    
    
    var presenter: StargazerPresenter!
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var reponameTextField: UITextField!
    @IBOutlet weak var searchButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    var spinner = UIActivityIndicatorView(style: .medium)
    var isLoading = false

    override func viewDidLoad() {
        super.viewDidLoad()
        self.presenter = StargazersPresenterImpl<SGZViewImpl>(view: self)
        //Register Loading Cell
        let tableViewLoadingCellNib = UINib(nibName: Constants.Nibs.LoadingCell, bundle: nil)
        self.tableView.register(tableViewLoadingCellNib, forCellReuseIdentifier: Constants.CellIdentifiers.tableviewloadingcell)
        self.prepareTableView()
        self.hideTableView()
    }
    
    func showLoading() {
        SpinnerAndMessage.sharedInstance.showSpinnerInView(view: self.view)
    }
    
    func hideLoading() {
        SpinnerAndMessage.sharedInstance.hideSpinnerInView(view: self.view)
    }
    
    func reloadData() {
        self.tableView.reloadData()
    }
    
    func showMessage(message: String) {
        SpinnerAndMessage.sharedInstance.showMessage(title: Constants.Strings.appName, message: message, viewController: self   )
    }
    
    func showTableView(){
        self.tableView.isHidden = false
    }
    
    func hideTableView() {
        self.tableView.isHidden = true
    }
    
    @IBAction func search(_ sender: Any) {
        if((usernameTextField.text?.isValid())! && (reponameTextField.text?.isValid())!){
            self.presenter.getData(username: usernameTextField.text, reponame: reponameTextField.text)
        }else{
            SpinnerAndMessage.sharedInstance.showMessage(title: Constants.Error.attention, message: Constants.Strings.invalidStrings, viewController: self)
        }
    }
}

//TableView Delegates
extension SGZViewImpl{
    
    fileprivate func prepareTableView(){
        self.tableView.delegate = self
        self.tableView.dataSource = self
        tableView.tableFooterView = UIView()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if indexPath.section == 0 {
            return 80 //Item Cell height
        } else {
            return 55 //Loading Cell height
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return presenter.numberOfSections()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            //Return the amount of items
            return presenter.numberOfRowsInSection(section: section)
        } else if section == 1 {
            //Return the Loading cell
            return 1
        } else {
            //Return nothing
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            return stargazerCellAtIndexPath(indexPath: indexPath)
         } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: Constants.CellIdentifiers.tableviewloadingcell, for: indexPath) as! LoadingCell
             cell.activityIndicator.startAnimating()
             return cell
         }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
            let offsetY = scrollView.contentOffset.y
            let contentHeight = scrollView.contentSize.height
            if (offsetY > contentHeight - scrollView.frame.height * 4) && !isLoading {
                self.loadMoreData()
            }
        }
    
    private func loadMoreData() {
            if !self.isLoading {
                self.isLoading = true
                DispatchQueue.global().async {
                    // Fake background loading task for 2 seconds
                    sleep(2)
                    self.presenter.loadNextPage()
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                        self.isLoading = false
                    }
                }
            }
        }
    
    fileprivate func stargazerCellAtIndexPath(indexPath: IndexPath)-> SGZCell {
        let stargazer = self.presenter.getObjectAt(index: indexPath)
        let cellIdentifier = Constants.CellIdentifiers.stargazerCell
        let cell = self.tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for:indexPath) as! SGZCell
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        cell.setTitle(value: stargazer.title)
        cell.setSubtitle(value: stargazer.subtitle)
        cell.setImageUrl(value: stargazer.imageUrl)
        return cell
    }
}

