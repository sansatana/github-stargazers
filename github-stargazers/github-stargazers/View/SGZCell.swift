//
//  StargazerCell.swift
//  github-stargazers
//
//  Created by Andrea Bozza on 04/04/2021.
//

import Foundation
import UIKit

class SGZCell: UITableViewCell{
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var subtitle: UILabel!
    @IBOutlet weak var profileImage: UIImageView!
    
    public func setTitle(value:String?){
        self.title.text = value ?? Constants.Strings.undefined
    }
    
    public func setSubtitle(value:String?){
        self.subtitle.text = value ?? Constants.Strings.undefined
    }
    
    public func setImageUrl(value:String?){
        self.profileImage.image = nil
        if let value = value {
            self.profileImage.downloadedFrom(link: value )
        }
    }
}

private extension UIImageView {
    private func downloadedFrom(url: URL, contentMode mode: UIView.ContentMode) {
        contentMode = mode
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
            else { return }
            DispatchQueue.main.async() { () -> Void in
                self.image = image
            }
        }.resume()
    }
    func downloadedFrom(link: String, contentMode mode: UIView.ContentMode = .scaleAspectFit) {
        guard let url = URL(string: link) else { return }
        downloadedFrom(url: url, contentMode: mode)
    }
}
