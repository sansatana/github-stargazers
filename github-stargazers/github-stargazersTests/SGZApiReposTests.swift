//
//  SGZApiReposTests.swift
//  github-stargazers
//
//  Created by Andrea Bozza on 02/04/2021.
//

import XCTest
@testable import github_stargazers
/**
 Mocking server call; mock URLSessionDataTask
 */

class MockTask: URLSessionDataTask {
    private let data: Data?
    private let urlResponse: URLResponse?
    private let _error: Error?
    override var error: Error? {
        return _error
    }
    
    var completionHandler: ((Data?, URLResponse?, Error?) -> Void)!
    
    init(data: Data?, urlResponse: URLResponse?, error: Error?) {
        self.data = data
        self.urlResponse = urlResponse
        self._error = error
    }
    override func resume() {
        DispatchQueue.main.async {
            self.completionHandler(self.data, self.urlResponse, self.error)
        }
    }
}

/**
 Mocking server call; mock URLSession
 */

class MockURLSession: URLSession {
    var cachedUrl: URL?
    private let mockTask: MockTask
    init(data: Data?, urlResponse: URLResponse?, error: Error?) {
        mockTask = MockTask(data: data, urlResponse: urlResponse, error:
                                error)
    }
    override func dataTask(with url: URL, completionHandler:      @escaping(Data?, URLResponse?, Error?) -> Void) -> URLSessionDataTask {
        self.cachedUrl = url
        mockTask.completionHandler = completionHandler
        return mockTask
    }
}


class SGZApiReposTests: XCTestCase {
    
    
    /**
     Get Stargazers From API Sets up URL not wrong
     */
    
    func testgetStargazersWithNullUrl() {
        let apiRespository =  SGZApiRepo()
        apiRespository.getStargazers(owner: "andrea", repo: "myproject") { result in
            switch result {
            case .failure(_):
                XCTFail("Success expected")
            case .success( _, _):
                XCTFail("Success expected")
                
            }
        }
    }
    
    /**
     Get Stargazers From API Sets up URL Host and Path as Expected
     */
    
    func testgetStargazersWithExpectedURLHostAndPath() {
        let apiRespository =  SGZApiRepo()
        let mockURLSession  = MockURLSession(data: nil, urlResponse: nil, error: nil)
        apiRespository.session = mockURLSession
        apiRespository.getStargazers(owner: "andrea", repo: "myproject") { result in
            
        }
        XCTAssertEqual(mockURLSession.cachedUrl?.host, "api.github.com")
        XCTAssertEqual(mockURLSession.cachedUrl?.path, "/repos/andrea/myproject/stargazers")
    }
    
    
    /**
     Get Stargazers From API Successfully Returns List of Stargazer
     */
    
    func testgetStargazersSuccessReturnsStargazer() {
        let apiRespository = SGZApiRepo()
        let jsonData = "[{\"login\": \"octocat\",\"avatar_url\": \"https://github.com/images/error/octocat_happy.gif\"}]".data(using: .utf8)
        let mockURLSession  = MockURLSession(data: jsonData, urlResponse: nil, error: nil)
        apiRespository.session = mockURLSession
        let stargazersExpectation = expectation(description: "stargazers")
        var stargazersResponse: Result<[Stargazer]>?
        apiRespository.getStargazers(owner: "andrea", repo: "myproject") { result in
            stargazersResponse = result
            stargazersExpectation.fulfill()
        }
        waitForExpectations(timeout: 1) { (error) in
            switch stargazersResponse {
            case .failure(_):
                XCTFail("Success expected")
            case .success(let stargazer, _):
                XCTAssertNotNil(stargazer)
                XCTAssertEqual(1, stargazer.count)
            case .none:
                XCTFail("Success expected")
            }
        }
    }
    
    /**
     Get Stargazers From API Fails and  Returns error
     */
    
    func testgetStargazersWhenResponseErrorReturnsError() {
        let apiRespository = SGZApiRepo()
        let error = NSError(domain: "error", code: 1234, userInfo: nil)
        let mockURLSession  = MockURLSession(data: nil, urlResponse: nil, error: error)
        
        apiRespository.session = mockURLSession
        let errorExpectation = expectation(description: "error")
        var stargazersResponse: Result<[Stargazer]>?
        apiRespository.getStargazers(owner: "andrea", repo: "myproject") { result in
            stargazersResponse = result
            errorExpectation.fulfill()
        }
        
        waitForExpectations(timeout: 1) { (error) in
            switch stargazersResponse {
            case .failure(let error):
                XCTAssertNotNil(error)
                XCTAssertEqual("Invalid response", error.localizedDescription)
            case .success(_, _):
                XCTFail("Failure expected")
            case .none:
                XCTFail("Success expected")
            }
        }
        
        
    }
    
    /**
     Get Stargazers From API Fails and  Returns error when receive no data
     */
    
    func testgetStargazersWhenEmptyDataReturnsError() {
        let apiRespository = SGZApiRepo()
        let mockURLSession  = MockURLSession(data: nil, urlResponse: nil, error: nil)
        apiRespository.session = mockURLSession
        let errorExpectation = expectation(description: "error")
        var stargazersResponse: Result<[Stargazer]>?
        apiRespository.getStargazers(owner: "andrea", repo: "myproject") { result in
            stargazersResponse = result
            errorExpectation.fulfill()
        }
        waitForExpectations(timeout: 1) { (error) in
            switch stargazersResponse {
            case .failure(let error):
                XCTAssertNotNil(error)
                XCTAssertEqual("No data", error.localizedDescription)
            case .success(_, _):
                XCTFail("Failure expected")
            case .none:
                XCTFail("Success expected")
            }
        }
    }
    
    /**
     Get Stargazers From API with Invalid JSON returns Error
     */
    
    func testgetStargazersInvalidJSONReturnsError() {
        let jsonData = "[{\"querty\"}]".data(using: .utf8)
        let apiRespository = SGZApiRepo()
        let mockURLSession  = MockURLSession(data: jsonData, urlResponse: nil, error: nil)
        apiRespository.session = mockURLSession
        let errorExpectation = expectation(description: "error")
        var stargazersResponse: Result<[Stargazer]>?
        apiRespository.getStargazers(owner: "andrea", repo: "myproject") { result in
            stargazersResponse = result
            errorExpectation.fulfill()
        }
        waitForExpectations(timeout: 1) { (error) in
            switch stargazersResponse {
            case .failure(let error):
                XCTAssertNotNil(error)
            case .success(_, _):
                XCTFail("Failure expected")
            case .none:
                XCTFail("Success expected")
            }
        }
    }
    
    /**
     Test valid paginaion url
     */
    
    func testGetValidPaginationURL(){
        let apiRespository = SGZApiRepo()
        let jsonData = "[{\"login\": \"octocat\",\"avatar_url\": \"https://github.com/images/error/octocat_happy.gif\"}]".data(using: .utf8)
        let urlResponse = HTTPURLResponse(url: URL(string: "https://api.github.com/repos/octocat/hello-world/stargazers")!, statusCode: 200, httpVersion: "", headerFields: ["link": "<https://api.github.com/repositories/1296269/stargazers?page=2>; rel=\"next\",<https://api.github.com/repositories/1296269/stargazers?page=55>; rel=\"last\""])
        let mockURLSession  = MockURLSession(data: jsonData, urlResponse: urlResponse, error: nil)
        apiRespository.session = mockURLSession
        var paginationURL: String?
        let errorExpectation = expectation(description: "error")
        var stargazersResponse: Result<[Stargazer]>?
        apiRespository.getStargazers(owner: "andrea", repo: "myproject") { result in
            switch result {
            case .failure(let error):
                XCTAssertNotNil(error)
            case .success(_, let response):
                let headerFields = (response as? HTTPURLResponse)?.allHeaderFields
                if let headerFields = headerFields{
                    paginationURL = getNextPageUrl(linkHeader: headerFields["Link"] as? String)
                }
                stargazersResponse = result
                errorExpectation.fulfill()
            }
            
        }
        
        waitForExpectations(timeout: 1) { (error) in
            switch stargazersResponse {
            case .failure(let error):
                XCTAssertNotNil(error)
            case .success(let stargazer, _):
                XCTAssertGreaterThan(stargazer.count, 0)
                XCTAssertEqual(paginationURL, "https://api.github.com/repositories/1296269/stargazers?page=2")
            case .none:
                XCTFail("Success expected")
            }
        }
    }
    
    /**
     Test last paginaion url
     */
    
    func testLastPaginationURL(){
        let apiRespository = SGZApiRepo()
        let jsonData = "[]".data(using: .utf8)
        let mockURLSession  = MockURLSession(data: jsonData, urlResponse: nil, error: nil)
        apiRespository.session = mockURLSession
        let errorExpectation = expectation(description: "error")
        var stargazersResponse: Result<[Stargazer]>?
        apiRespository.getNextStargazers(nextPage: URL(string: "https://api.github.com/repositories/1296269/stargazers?page=2300000")!) { result in
            stargazersResponse = result
            errorExpectation.fulfill()
        }
        waitForExpectations(timeout: 1) { (error) in
            switch stargazersResponse {
            case .failure(let error):
                XCTAssertNotNil(error)
            case .success(let stargazer, _):
                XCTAssertEqual(stargazer.count, 0)
            case .none:
                XCTFail("Success expected")
            }
        }
        
    }
    
    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
}

private enum MyError: Error {
    case mocked
}
