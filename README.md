# Github-Stargazer

Github-Stargazer provide an interface to Stargazer github API. You can search stargazers of repository with Owner and Repository  to view the list of stargazers of a GitHub repository.

Design pattern: MVP
No external library (avoid use of CocoaPods or Carthage)


Next feature to develop:
- implements authentication to avoid the api limit defined by this header:
-- x-ratelimit-limit: 60
-- x-ratelimit-remaining: 0
- provide better pagination
- add more log
- ...

See documentation:
https://docs.github.com/en/rest/overview/resources-in-the-rest-api
